<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $name = $_POST['name'];
    // $created_at = NOW();
    // $updated_at = NOW();

    $cek = "SELECT * FROM categories WHERE cat_name='$name'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if (isset($result)) {
        $response['value'] = 2;
        $response['msg'] = 'Kategori sudah tersedia';
        echo json_encode($response);
    } else {
        $insert = "INSERT INTO categories VALUE(NULL,'$name',1,NOW(),NOW())";
        if (mysqli_query($connect, $insert)) {
            $response['value'] = 1;
            $response['msg'] = 'Berhasil ditambahkan';
            echo json_encode($response);
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal ditambahkan';
            echo json_encode($response);
        }
    }
}
