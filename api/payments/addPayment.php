<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $order_id = $_POST['order_id'];
    $total = $_POST['pay_total'];
    $discount = $_POST['pay_discount'];
    $payment = $_POST['payment'];
    $kurang = $_POST['pay_kurang'];
    $lunas = $_POST['lunas'];

    // $created_at = NOW();
    // $updated_at = NOW();

    if ($lunas == "0") {
        $insert = "INSERT INTO payments VALUE(NULL,'$order_id','$total','$discount','$payment','$kurang','0','0',NOW(),NULL)";
        if (mysqli_query($connect, $insert)) {
            $response['value'] = 1;
            $response['msg'] = 'Berhasil ditambahkan';
            echo json_encode($response);
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal ditambahkan';
            echo json_encode($response);
        }
    } else {
        $insert = "INSERT INTO payments VALUE(NULL,'$order_id','$total','$discount','$payment','0','$kurang','1',NOW(),NULL)";
        if (mysqli_query($connect, $insert)) {
            $response['value'] = 1;
            $response['msg'] = 'Berhasil ditambahkan';
            echo json_encode($response);
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal ditambahkan';
            echo json_encode($response);
        }
    }
}
