<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $order_id = $_POST['order_id'];
    $total = $_POST['pay_total'];
    $discount = $_POST['pay_discount'];
    $payment = $_POST['payment'];
    $kurang = $_POST['pay_kurang'];
    $lunas = $_POST['lunas'];
    // $created_at = NOW();
    // $updated_at = NOW();

    $cek = "SELECT * FROM payments WHERE order_id='$order_id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        if ($lunas == "0") {
            $insert = "UPDATE payments SET pay_total='$total',pay_discount='$discount',payment='$payment',pay_kurang='$kurang',lunas='$lunas', updated_at= NULL WHERE order_id = '$order_id'";

            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        } else {
            $insert = "UPDATE payments SET pay_total='$total',pay_discount='$discount',payment='$payment',pay_kurang='0',repayment='$kurang',lunas='$lunas', updated_at= NULL WHERE order_id = '$order_id'";

            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        }
    }
}
