<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $response = array();

    $sql = "SELECT COALESCE(sum(payment+repayment),0) AS total_payment FROM payments";
    
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['total_payment'];

    if ($total == NULL) {
        $response['total_payment'] = "0";
        echo json_encode($response);
    } else {
        $response['total_payment'] = $result['total_payment'];
        echo json_encode($response);
    }

}
