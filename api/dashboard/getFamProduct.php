<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {

    $response = array();

    $detQuery = "SELECT p.id, p.prod_name, p.prod_picture, p.prod_counter, c.cat_name FROM products p LEFT JOIN categories c ON p.cat_id=c.id  ORDER BY p.prod_counter DESC";

    $resultData = mysqli_query($connect, $detQuery);
    while ($detRecord = mysqli_fetch_assoc($resultData)) {
        $response[] = $detRecord;
    }
    echo json_encode($response);
}
