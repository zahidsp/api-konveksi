<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $cat_id = $_POST['cat_id'];
    $prod_name = $_POST['prod_name'];
    $prod_price = $_POST['prod_price'];
    $prod_counter = $_POST['prod_counter'];
    // $prod_visible = $_POST['prod_visible'];

    $cek = "SELECT * FROM categories WHERE id='$cat_id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        $prod_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['prod_picture']['name']));
        $picturePath = "../../uploads/images/" . $prod_picture;
        move_uploaded_file($_FILES['prod_picture']['tmp_name'], $picturePath);
        
        $insert = "INSERT INTO products VALUE(NULL,'$cat_id','$prod_name','$prod_picture','$prod_price',0,1,NOW(),NOW())";
        if (mysqli_query($connect, $insert)) {
            $response['value'] = 1;
            $response['msg'] = 'Berhasil disimpan';
            echo json_encode($response);
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal disimpan';
            echo json_encode($response);
        }
    }

    // $created_at = NOW();
    // $updated_at = NOW();


}
