<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $catId = $_GET['id'];

    $cek = "SELECT * FROM products WHERE cat_id = $catId";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == null) {
        $response['value'] = 2;
        $response['msg'] = 'Data tidak tersedia';
        echo json_encode($response);
    } else {
        getProduct($catId, $connect);
       
    }
}

function getProduct($catId, $conn)
{
    $sqlQuery = '';
    if ($catId) {
        $sqlQuery = "WHERE cat_id = '" . $catId . "'";
    }
    $prdQuery = "
    SELECT p.*, c.cat_name FROM products p LEFT JOIN categories c ON p.cat_id=c.id $sqlQuery ";
    // ORDER BY id DESC";
    $resultData = mysqli_query($conn, $prdQuery);
    $prdData = array();
    while ($prdRecord = mysqli_fetch_assoc($resultData)) {
        $prdData[] = $prdRecord;
    }

    echo json_encode($prdData);
}
