<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $editedImage = $_POST['editedImage'];
    $id = $_POST['id'];
    $cat_id = $_POST['cat_id'];
    $prod_name = $_POST['prod_name'];
    $prod_price = $_POST['prod_price'];
    // $prod_picture = $_POST['prod_picture'];
    // $prod_visible = $_POST['prod_visible'];
    // $created_at = NOW();
    // $updated_at = NOW();

    $response = array();

    $cek = "SELECT * FROM products WHERE id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        if ($editedImage == "true") {
            $prod_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['prod_picture']['prod_name']));
            $picturePath = "../../uploads/images/" . $prod_picture;
            move_uploaded_file($_FILES['prod_picture']['tmp_name'], $picturePath);

            $insert = "UPDATE products SET cat_id='$cat_id', prod_name='$prod_name', prod_picture='$prod_picture', prod_price='$prod_price',updated_at = NOW() WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        } else {
            $insert = "UPDATE products SET cat_id='$cat_id', prod_name='$prod_name', prod_price='$prod_price',updated_at = NOW() WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        }
    }
}
