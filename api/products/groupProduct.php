<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $id = $_GET['id'];
    $id = mysqli_real_escape_string($connect,$id);

    $response = array();

    $sql = mysqli_query($connect, "SELECT p.*, c.cat_name FROM products p LEFT JOIN categories c ON p.cat_id=c.id WHERE p.cat_id='$id'");
    while ($a = mysqli_fetch_array($sql)) {
        $p['id'] = $a['id'];
        $p['cat_id'] = $a['cat_id'];
        $p['prod_name'] = $a['prod_name'];
        $p['prod_picture'] = $a['prod_picture'];
        $p['prod_price'] = $a['prod_price'];
        $p['prod_counter'] = $a['prod_counter'];
        $p['created_at'] = $a['created_at'];
        $p['updated_at'] = $a['updated_at'];
        $p['cat_name'] = $a['cat_name'];

        array_push($response, $p);
    }

    echo json_encode($response);
}
