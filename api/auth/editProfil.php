<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $editedImage = $_POST['editedImage'];
    $id = $_POST['id'];
    $name = $_POST['name'];
    $email = $_POST['email'];

    $response = array();

    $cek = "SELECT * FROM users WHERE id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        if ($editedImage == "true") {
            $picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['picture']['name']));
            $picturePath = "../../uploads/images/" . $picture;
            move_uploaded_file($_FILES['picture']['tmp_name'], $picturePath);

            $insert = "UPDATE users SET name='$name', email='$email', picture='$picture', updated_at = NOW() WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        } else {
            $insert = "UPDATE users SET name='$name', email='$email', updated_at = NOW() WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        }
    }
}
