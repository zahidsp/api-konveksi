<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $editedImage = $_POST['editedImage'];
    $id = $_POST['id'];
    $suplier_id = $_POST['suplier_id'];

    $shop_note = $_POST['shop_note'];
    $shop_total = $_POST['shop_total'];
    $shop_discount = $_POST['shop_discount'];
    $profit = $_POST['profit'];
    // $shop_picture = $_POST['shop_picture'];
    // $created_at = NOW();
    // $updated_at = NOW();

    $cek = "SELECT * FROM productions WHERE id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        if ($editedImage == "true") {
            $shop_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['shop_picture']['name']));
            $picturePath = "../../uploads/images/" . $shop_picture;
            move_uploaded_file($_FILES['shop_picture']['tmp_name'], $picturePath);

            $insert = "UPDATE productions SET suplier_id='$suplier_id', shop_note='$shop_note',shop_total='$shop_total',shop_discount='$shop_discount',profit='$profit', shop_picture='$shop_picture', updated_at= NOW() WHERE id = '$id'";

            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        } else {
            $insert = "UPDATE productions SET suplier_id='$suplier_id', shop_note='$shop_note',shop_total='$shop_total',shop_discount='$shop_discount',profit='$profit',updated_at= NOW() WHERE id = '$id'";

            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        }
    }
}
