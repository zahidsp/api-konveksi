<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();

    $id = $_POST['id'];

    $cek = "SELECT * FROM productions WHERE order_id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        $insert = "DELETE FROM productions WHERE order_id = '$id'";
        if (mysqli_query($connect, $insert)) {
            $insert = "UPDATE orders SET ord_status='1', updated_at = NOW() WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil di hapus';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal di hapus';
                echo json_encode($response);
            }
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal dihapus';
            echo json_encode($response);
        }
    }
}
