<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $id = $_GET['id'];

    $prdQuery = "
    SELECT s.* FROM productions p LEFT JOIN supliers s ON p.suplier_id=s.id WHERE p.id=$id";
    $resultData = mysqli_fetch_array(mysqli_query($connect, $prdQuery));

    $ordData = array();
    $ordData['id'] = $resultData['id'];
    $ordData['sup_name'] = $resultData['sup_name'];
    $ordData['sup_address'] = $resultData['sup_address'];
    $ordData['sup_phone'] = $resultData['sup_phone'];

    echo json_encode($ordData);
}
