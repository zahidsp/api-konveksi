<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $order_id = $_POST['order_id'];
    $suplier_id = $_POST['suplier_id'];
    $shop_note = $_POST['shop_note'];
    $shop_total = $_POST['shop_total'];
    $shop_discount = $_POST['shop_discount'];
    $profit = $_POST['profit'];

    // $shop_picture = $_POST['shop_picture'];

    $shop_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['shop_picture']['name']));
    $picturePath = "../../uploads/images/" . $shop_picture;
    move_uploaded_file($_FILES['shop_picture']['tmp_name'], $picturePath);

    $insert = "INSERT INTO productions VALUE(NULL,'$order_id','$suplier_id','$shop_note','$shop_total','$shop_discount','$profit','$shop_picture', NOW(), NOW())";
    if (mysqli_query($connect, $insert)) {
        $production_id = $connect->insert_id;

        $order = "UPDATE orders SET ord_status='2', updated_at = NOW() WHERE id = '$order_id'";
        mysqli_query($connect, $order);
        
        $response['value'] = 1;
        $response['msg'] = 'Berhasil ditambahkan';
        echo json_encode($response);
    } else {
        $response['value'] = 0;
        $response['msg'] = 'Gagal ditambahkan';
        echo json_encode($response);
    }
}
