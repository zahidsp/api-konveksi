<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $month = $_GET['month'];
    $year = $_GET['year'];
    $response = array();

    // $sql = "SELECT o.ord_name, p.shop_total,p.shop_discount,p.profit FROM orders o LEFT JOIN productions p on o.id=p.order_id  WHERE MONTH(o.created_at) = $month  AND YEAR(o.created_at) = $year";
    $sql = "SELECT o.ord_name, p.shop_total,p.shop_discount,p.profit,p.created_at FROM orders o LEFT JOIN productions p on o.id=p.order_id WHERE p.shop_total IS NOT NULL AND MONTH(p.created_at) = $month AND YEAR(p.created_at)=$year";
    $resultData = mysqli_query($connect, $sql);
    while ($detRecord = mysqli_fetch_assoc($resultData)) {
        $response[] = $detRecord;
    }
    echo json_encode($response);
}
