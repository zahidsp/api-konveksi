<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $month = $_GET['month'];
    $year = $_GET['year'];
    $response = array();

    $sql = "SELECT COALESCE(sum(shop_total-shop_discount),0) as shop_total FROM productions WHERE MONTH(created_at) = $month  AND YEAR(created_at) = $year";
    
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['shop_total'];
    
    if ($total == NULL) {
        $response['shop_total'] = 0;
        echo json_encode($response);
    } else {
        $response['shop_total'] = $result['shop_total'];
        echo json_encode($response);
    }
}
