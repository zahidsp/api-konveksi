<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $month = $_GET['month'];
    $year = $_GET['year'];
    $response = array();

    $sql = "SELECT COALESCE(sum(p.payment),0) as total_payment FROM orders o LEFT JOIN payments p ON o.id=p.order_id WHERE MONTH(o.created_at) = $month  AND YEAR(o.created_at) = $year";
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $totalPayment = $result['total_payment'];

    $sql = "SELECT COALESCE(sum(repayment),0) as repayment FROM payments WHERE MONTH(updated_at) = $month  AND YEAR(updated_at) = $year";
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['repayment'] + $totalPayment;

    if ($total == NULL) {
        $response['total_payment'] = "0";
        echo json_encode($response);
    } else {
        $response['total_payment'] = "$total";
        echo json_encode($response);
    }
}
