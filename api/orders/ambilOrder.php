<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();

    $id = $_POST['id'];
    $ord_status = $_POST['ord_status'];

    $cek = "SELECT * FROM orders WHERE id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {

        $insert = "UPDATE orders SET ord_status=$ord_status, updated_at = NOW() WHERE id = '$id'";

        if (mysqli_query($connect, $insert)) {
            $response['value'] = 1;
            $response['msg'] = 'Berhasil diupdate';
            echo json_encode($response);
        } else {
            $response['value'] = 0;
            $response['msg'] = 'Gagal diupdate';
            echo json_encode($response);
        }
    }
}
