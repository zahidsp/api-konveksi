<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $orderId = $_GET['id'];

    $prdQuery = "SELECT o.*,p.pay_total,p.pay_discount,p.payment,p.pay_kurang,p.lunas FROM orders o LEFT JOIN payments p ON o.id=p.order_id WHERE o.id= $orderId";

    // ORDER BY id DESC";
    $resultData = mysqli_query($connect, $prdQuery);
    $ordData = array();
    while ($ordRecord = mysqli_fetch_assoc($resultData)) {
        $ordData[] = $ordRecord;
    }

    echo json_encode($ordData);
}
