<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $orderId = $_GET['id'];

    $cek = "SELECT * FROM orders WHERE id = $orderId";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == null) {
        $response['value'] = 2;
        $response['msg'] = 'Data tidak tersedia';
        echo json_encode($response);
    } else {
        getClient($orderId, $connect);
    }
}

function getClient($orderId, $conn)
{
    $prdQuery = "
    SELECT c.* FROM orders o LEFT JOIN clients c ON o.client_id=c.id WHERE o.id=$orderId";
    $resultData = mysqli_fetch_array(mysqli_query($conn, $prdQuery));

    $ordData = array();
    $ordData['id'] = $resultData['id'];
    $ordData['client_name'] = $resultData['client_name'];
    $ordData['client_address'] = $resultData['client_address'];
    $ordData['client_phone'] = $resultData['client_phone'];

    echo json_encode($ordData);
}
