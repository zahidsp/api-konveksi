<?php

require '../../config/connect.php';

$queryresponse = $connect->query("SELECT * FROM orders");
$response = array();

$cek = "SELECT * FROM orders";
$result = mysqli_fetch_array(mysqli_query($connect, $cek));

if ($result == null) {
    $response['value'] = 2;
    $response['msg'] = 'Data tidak tersedia';
    echo json_encode($response);
} else {
    while ($fetchData = $queryresponse->fetch_assoc()) {
        $response[] = $fetchData;
    }
    // $response['value'] = 1;
    // $response['msg'] = 'Data tersedia';

    echo json_encode($response);
}
