<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();

    $editedImage = $_POST['editedImage'];
    $id = $_POST['id'];
    $client_id = $_POST['client_id'];
    $ord_name = $_POST['ord_name'];
    $ord_note = $_POST['ord_note'];
    // $ord_picture = $_POST['ord_picture'];
    // $ord_status = $_POST['ord_status'];
    $tanggal_jadi = $_POST['tanggal_jadi'];
    // $created_at = $_POST['created_at'];
    // $updated_at = NOW();
    $cek = "SELECT * FROM orders WHERE id='$id'";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == NULL) {
        $response['value'] = 2;
        $response['msg'] = 'data tidak tersedia';
        echo json_encode($response);
    } else {
        if ($editedImage == "true") {
            $ord_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['ord_picture']['name']));
            $picturePath = "../../uploads/images/" . $ord_picture;
            move_uploaded_file($_FILES['ord_picture']['tmp_name'], $picturePath);

            $insert = "UPDATE orders SET client_id='$client_id', ord_name='$ord_name', ord_note='$ord_note', ord_picture='$ord_picture', updated_at = NOW(), tanggal_jadi='$tanggal_jadi' WHERE id = '$id'";
            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        } else {
            $insert = "UPDATE orders SET client_id='$client_id', ord_name='$ord_name', ord_note='$ord_note', updated_at = NOW(), tanggal_jadi='$tanggal_jadi' WHERE id = '$id'";

            if (mysqli_query($connect, $insert)) {
                $response['value'] = 1;
                $response['msg'] = 'Berhasil diupdate';
                echo json_encode($response);
            } else {
                $response['value'] = 0;
                $response['msg'] = 'Gagal diupdate';
                echo json_encode($response);
            }
        }
    }
}
