<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {

    $response = array();
    $user_id = $_POST['user_id'];
    $client_id = $_POST['client_id'];
    $invoice_no = $_POST['invoice_no'];
    $ord_name = $_POST['ord_name'];
    $ord_note = $_POST['ord_note'];
    $tanggal_jadi = $_POST['tanggal_jadi'];
    $created_at = $_POST['createdAt'];
    // $updated_at = NOW();

    $ord_picture = date('dmYHis') . str_replace(" ", "", basename($_FILES['ord_picture']['name']));
    $picturePath = "../../uploads/images/" . $ord_picture;
    move_uploaded_file($_FILES['ord_picture']['tmp_name'], $picturePath);

    $insert = "INSERT INTO orders (user_id, client_id, invoice_no, ord_name, ord_note, ord_picture, created_at, updated_at, tanggal_jadi) VALUES ('$user_id','$client_id','$invoice_no','$ord_name','$ord_note','$ord_picture','$created_at','$created_at', '$tanggal_jadi')";

    if (mysqli_query($connect, $insert)) {
        $order_id = $connect->insert_id;
        $response['value'] = 1;
        $response['order_id'] = $order_id;
        $response['msg'] = 'Berhasil ditambahkan';
        echo json_encode($response);
    } else {
        $response['value'] = 0;
        $response['msg'] = 'Gagal ditambahkan';
        echo json_encode($response);
    }
}
