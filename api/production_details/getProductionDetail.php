<?php

require '../../config/connect.php';

if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $production_id = $_GET['id'];

    $cek = "SELECT * FROM production_details WHERE production_id= $production_id";
    $result = mysqli_fetch_array(mysqli_query($connect, $cek));

    if ($result == null) {
        $response['value'] = 2;
        $response['msg'] = 'Data tidak tersedia';
        echo json_encode($response);
    } else {
        getDetail($production_id, $connect);
    }
}

function getDetail($shop_id, $conn)
{
    $detQuery = "SELECT * FROM production_details WHERE production_id='$shop_id'";
    // ORDER BY id DESC";
    $resultData = mysqli_query($conn, $detQuery);
    $detData = array();
    while ($detRecord = mysqli_fetch_assoc($resultData)) {
        $detData[] = $detRecord;
    }

    echo json_encode($detData);
}
