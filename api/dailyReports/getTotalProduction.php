<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $date = $_GET['date'];

    $response = array();
    $sql = "SELECT COALESCE(sum(shop_total-shop_discount),0) AS shop_total FROM productions WHERE DATE(created_at)= '" . $date . "' OR DATE(updated_at)= '" . $date . "'";
   
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['shop_total'];

    if ($total == NULL) {
        $response['shop_total'] = 0;
        echo json_encode($response);
    } else {
        $response['shop_total'] = $result['shop_total'];
        echo json_encode($response);
    }
}
