<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $date = $_GET['date'];
    $response = array();

    $sql = "SELECT COALESCE(sum(p.payment),0) AS total_payment FROM orders o lEFT JOIN payments p ON o.id=p.order_id WHERE DATE(o.created_at)= '" . $date . "'";
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $totalPayment = $result['total_payment'];

    $sql = "SELECT COALESCE(sum(repayment),0) AS repayment FROM payments WHERE DATE(updated_at)= '" . $date . "'";
    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['repayment'] + $totalPayment;

    if ($total == NULL) {
        $response['total_payment'] = 0;
        echo json_encode($response);
    } else {
        $response['total_payment'] = $total;
        echo json_encode($response);
    }
}
