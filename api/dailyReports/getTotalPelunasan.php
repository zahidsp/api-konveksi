<?php

require '../../config/connect.php';
if ($_SERVER['REQUEST_METHOD'] == "GET") {
    $date = $_GET['date'];
    $response = array();

    $sql = "SELECT COALESCE(sum(repayment),0) AS repayment FROM payments WHERE DATE(updated_at)= '" . $date . "'";

    $result = mysqli_fetch_array(mysqli_query($connect, $sql));
    $total = $result['repayment'];

    if ($total == NULL) {
        $response['repayment'] = 0;
        echo json_encode($response);
    } else {
        $response['repayment'] = $result['repayment'];
        echo json_encode($response);
    }
}
