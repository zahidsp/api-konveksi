-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2019 at 09:22 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_konveksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_visible`, `created_at`, `updated_at`) VALUES
(1, 'Kaos', 1, '2019-12-03 14:27:09', '2019-12-04 04:29:48'),
(2, 'Kemeja', 1, '2019-12-03 22:54:49', '2019-12-03 22:54:49'),
(3, 'Jaket', 1, '2019-12-03 23:56:19', '2019-12-06 12:53:11'),
(4, 'Blazer', 1, '2019-12-03 23:57:41', '2019-12-03 23:57:41'),
(5, 'Varsity', 1, '2019-12-03 23:59:33', '2019-12-04 04:33:41');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_counter` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `client_address`, `client_phone`, `client_counter`, `created_at`, `updated_at`) VALUES
(1, 'Dilan Ramadhan', 'Seyegan, Sleman', '0897632343923', 0, '2019-12-05 06:17:49', '2019-12-05 15:33:43'),
(2, 'Wulandari', 'Mlati, Sleman', '0856432109645', 0, '2019-12-05 06:18:43', '2019-12-05 15:33:23'),
(3, 'Cipto Putra', 'Piyungan, Bantul', '085643987345', 0, '2019-12-05 06:19:09', '2019-12-05 15:32:46'),
(4, 'Indri A', 'Banguntapan, Bantul', '08343872391240', 0, '2019-12-05 06:19:33', '2019-12-05 06:24:18'),
(9, 'Tri Raharjo', 'Kalasan, Sleman', '85645258987', 0, '2019-12-05 15:35:47', '2019-12-05 15:36:04'),
(10, 'Irawan Sentosa', 'Sewon, Bantul', '0897654359234', 0, '2019-12-13 16:16:21', '2019-12-13 16:44:32');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_18_045517_create_categories_table', 1),
(4, '2019_11_18_045802_create_products_table', 1),
(5, '2019_11_18_050009_create_clients_table', 1),
(6, '2019_11_18_050118_create_supliers_table', 1),
(7, '2019_11_18_050501_create_orders_table', 1),
(8, '2019_11_18_051320_create_order_details_table', 1),
(9, '2019_11_18_051520_create_payments_table', 1),
(10, '2019_11_18_053638_create_order_detail_product_table', 1),
(11, '2019_11_19_071112_create_productions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ord_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ord_note` text COLLATE utf8mb4_unicode_ci,
  `ord_picture` text COLLATE utf8mb4_unicode_ci,
  `ord_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tanggal_jadi` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `client_id`, `invoice_no`, `ord_name`, `ord_note`, `ord_picture`, `ord_status`, `created_at`, `updated_at`, `tanggal_jadi`) VALUES
(2, 1, 3, 'INV00940JF4T4T', 'Jaket Joss', 'ini tes boyy edited', '06122019151238scaled_1574155312216.jpg', 2, '2019-01-23 17:00:00', '2019-12-14 14:18:57', '2019-12-23 17:00:00'),
(3, 1, 2, 'INV0543543K5345', 'Blazer Hoy', 'ini tes boyy 3', '06122019151255scaled_1571583997430.jpg', 3, '2019-12-14 14:20:39', '2019-12-14 14:20:39', '2019-12-23 17:00:00'),
(19, 1, 3, 'INVORD121720192817', 'Uikkk', 'yui', '17122019091857scaled_Xiaomi-Mi-9-Stock-Wallpaper-19-1080x2340.jpg', 1, '2019-12-17 08:18:33', '2019-12-17 08:18:57', '2019-12-17 08:18:33');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(10) UNSIGNED NOT NULL,
  `item_price` int(10) UNSIGNED NOT NULL,
  `item_quantity` int(10) UNSIGNED NOT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `item_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `prod_id`, `item_price`, `item_quantity`, `item_total`, `item_note`, `created_at`, `updated_at`) VALUES
(6, 2, 5, 50000, 50, 2500000, 'wwwwkooi', '2019-12-15 01:11:45', '2019-12-15 01:11:45'),
(8, 3, 7, 50000, 50, 2500000, 'ini 3', '2019-12-15 01:12:12', '2019-12-15 01:12:12'),
(10, 2, 7, 50000, 50, 2500000, 'ini 3', '2019-12-17 04:28:40', '2019-12-17 04:28:40'),
(64, 2, 7, 50000, 24, 2500000, 'ini 3', '2019-12-17 07:48:56', '2019-12-17 07:48:56'),
(81, 19, 11, 64000, 23, 1472000, 'yu', '2019-12-17 08:19:14', '2019-12-17 08:19:14'),
(82, 19, 8, 80000, 25, 2000000, 'gb', '2019-12-17 08:19:31', '2019-12-17 08:19:31');

-- --------------------------------------------------------

--
-- Table structure for table `order_detail_product`
--

CREATE TABLE `order_detail_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `detail_id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `pay_total` int(10) UNSIGNED NOT NULL,
  `pay_discount` int(10) UNSIGNED NOT NULL,
  `payment` int(10) UNSIGNED NOT NULL,
  `pay_kurang` int(10) UNSIGNED NOT NULL,
  `lunas` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `pay_total`, `pay_discount`, `payment`, `pay_kurang`, `lunas`, `created_at`, `updated_at`) VALUES
(2, 2, 4500000, 0, 2000000, 2500000, 0, '2019-12-14 05:36:26', '2019-12-14 05:36:26'),
(3, 3, 4700000, 0, 2000000, 2700000, 0, '2019-12-14 14:21:36', '2019-12-14 14:21:36'),
(8, 19, 3447000, 25000, 2000000, 1447000, 0, '2019-12-17 08:19:52', '2019-12-17 08:19:52');

-- --------------------------------------------------------

--
-- Table structure for table `productions`
--

CREATE TABLE `productions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `suplier_id` int(10) UNSIGNED NOT NULL,
  `shop_note` text COLLATE utf8mb4_unicode_ci,
  `shop_total` int(10) UNSIGNED NOT NULL,
  `shop_discount` int(10) UNSIGNED NOT NULL,
  `profit` int(10) UNSIGNED NOT NULL,
  `shop_picture` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `production_details`
--

CREATE TABLE `production_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `production_id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_quantity` int(10) UNSIGNED NOT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `item_note` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `prod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_picture` text COLLATE utf8mb4_unicode_ci,
  `prod_price` int(10) UNSIGNED NOT NULL,
  `prod_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `prod_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `prod_name`, `prod_picture`, `prod_price`, `prod_counter`, `prod_visible`, `created_at`, `updated_at`) VALUES
(5, 1, 'Hayate Kaos', '06122019151205scaled_1574156544889.jpg', 88888, 0, 1, '2019-12-06 14:12:05', '2019-12-14 05:24:09'),
(7, 2, 'Omen', '06122019151238scaled_1574155312216.jpg', 69000, 24, 1, '2019-12-06 14:12:38', '2019-12-17 07:48:57'),
(8, 3, 'Ryoma', '06122019151255scaled_1571583997430.jpg', 80000, 25, 1, '2019-12-06 14:12:55', '2019-12-17 08:19:31'),
(10, 4, 'Slimss', '06122019151329scaled_1568517588092.jpg', 50000, 0, 1, '2019-12-06 14:13:29', '2019-12-06 14:13:29'),
(11, 5, 'Arum', '06122019153137scaled_1568490972680.jpg', 64000, 23, 1, '2019-12-06 14:31:37', '2019-12-17 08:19:14'),
(13, 4, 'Blazer Cuyy', '16122019052317scaled_Screenshot_20191214-061158.png', 320000, 0, 1, '2019-12-16 04:23:17', '2019-12-16 04:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `supliers`
--

CREATE TABLE `supliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `sup_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_counter` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supliers`
--

INSERT INTO `supliers` (`id`, `sup_name`, `sup_address`, `sup_phone`, `sup_counter`, `created_at`, `updated_at`) VALUES
(1, 'Toko Jaya Abadi', 'Gejayan, Yogyakarta', '0274569845', 0, '2019-12-05 06:35:34', '2019-12-05 06:40:19'),
(3, 'Toko Semar Busana', 'Piyungan, Bantul', '02745694264', 0, '2019-12-05 06:36:57', '2019-12-05 06:41:49'),
(7, 'Toko Kain Abadi', 'Jalan Bridgen Katamso, Yogyakarta', '027456897', 0, '2019-12-05 16:35:15', '2019-12-05 16:35:15'),
(8, 'Toko Mantul', 'Piyungan, Bantul', '027422558', 0, '2019-12-13 16:57:00', '2019-12-13 17:16:10');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `picture`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'asdjsanmdo39eu3hri3nr32n43ne', 'admin@email.com', '21232f297a57a5a743894a0e4a801fc3', '2019-12-03 14:01:56', '2019-12-03 14:01:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_client_id_foreign` (`client_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_prod_id_foreign` (`prod_id`);

--
-- Indexes for table `order_detail_product`
--
ALTER TABLE `order_detail_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Indexes for table `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productions_order_id_foreign` (`order_id`),
  ADD KEY `productions_suplier_id_foreign` (`suplier_id`);

--
-- Indexes for table `production_details`
--
ALTER TABLE `production_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_production_id` (`production_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_cat_id_foreign` (`cat_id`);

--
-- Indexes for table `supliers`
--
ALTER TABLE `supliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=83;

--
-- AUTO_INCREMENT for table `order_detail_product`
--
ALTER TABLE `order_detail_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `productions`
--
ALTER TABLE `productions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `production_details`
--
ALTER TABLE `production_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `supliers`
--
ALTER TABLE `supliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_prod_id_foreign` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productions`
--
ALTER TABLE `productions`
  ADD CONSTRAINT `productions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `productions_suplier_id_foreign` FOREIGN KEY (`suplier_id`) REFERENCES `supliers` (`id`);

--
-- Constraints for table `production_details`
--
ALTER TABLE `production_details`
  ADD CONSTRAINT `fk_production_id` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
