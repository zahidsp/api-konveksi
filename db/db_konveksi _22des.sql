-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2019 at 02:50 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_konveksi`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cat_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_visible`, `created_at`, `updated_at`) VALUES
(17, 'Kaos', 1, '2019-12-18 05:41:04', '2019-12-18 05:44:53'),
(18, 'Kemeja', 1, '2019-12-18 05:41:52', '2019-12-18 05:45:18'),
(19, 'Jaket', 1, '2019-12-18 05:41:58', '2019-12-18 05:45:44'),
(22, 'Jas', 1, '2019-12-20 05:50:14', '2019-12-20 05:50:14'),
(23, 'Blazer', 1, '2019-12-20 05:50:20', '2019-12-20 05:50:20'),
(24, 'Raglan', 1, '2019-12-20 09:04:18', '2019-12-20 09:04:18');

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client_counter` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_name`, `client_address`, `client_phone`, `client_counter`, `created_at`, `updated_at`) VALUES
(1, 'Putri', 'Moyudan', '0853256980', 0, '2019-12-18 06:03:57', '2019-12-18 06:06:57'),
(2, 'Putra', 'Kota Gede', '085643258985', 0, '2019-12-18 06:06:29', '2019-12-18 06:07:21'),
(3, 'Bayu', 'Mlati', '085621758991', 0, '2019-12-20 10:08:16', '2019-12-20 10:08:16'),
(5, 'Andri', 'Pugeran', '08569521444', 0, '2019-12-20 13:15:13', '2019-12-20 13:15:13'),
(6, 'Joni', 'Kalasan', '083254125887', 0, '2019-12-21 21:43:28', '2019-12-21 21:54:38'),
(10, 'Eni', 'Singosaren', '085729655412', 0, '2019-12-21 21:47:09', '2019-12-21 21:54:15');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_11_18_045517_create_categories_table', 1),
(4, '2019_11_18_045802_create_products_table', 1),
(5, '2019_11_18_050009_create_clients_table', 1),
(6, '2019_11_18_050118_create_supliers_table', 1),
(7, '2019_11_18_050501_create_orders_table', 1),
(8, '2019_11_18_051320_create_order_details_table', 1),
(9, '2019_11_18_051520_create_payments_table', 1),
(10, '2019_11_18_053638_create_order_detail_product_table', 1),
(11, '2019_11_19_071112_create_productions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `invoice_no` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ord_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ord_note` text COLLATE utf8mb4_unicode_ci,
  `ord_picture` text COLLATE utf8mb4_unicode_ci,
  `ord_status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `tanggal_jadi` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `client_id`, `invoice_no`, `ord_name`, `ord_note`, `ord_picture`, `ord_status`, `created_at`, `updated_at`, `tanggal_jadi`) VALUES
(25, 1, 1, 'INVORD12222019110', 'Kaos Cuy', 'normal', '22122019051839scaled_Screenshot_20190710-052440_1.jpg', 2, '2019-12-22 04:17:46', '2019-12-22 07:48:41', '2019-01-23 17:00:00'),
(32, 1, 6, 'INVORD122220196713', 'Yy', 'cc', '22122019085531scaled_1574263645132.jpg', 2, '2019-12-20 17:00:00', '2019-12-22 07:57:09', '2019-12-22 07:55:18'),
(33, 1, 2, 'INVORD122220197552', 'Yyy', 'gg', '22122019090250scaled_1574584046904.jpg', 1, '2019-12-18 17:00:00', '2019-12-18 17:00:00', '2019-12-22 17:00:00'),
(34, 1, 5, 'INVORD122220191489', 'Kaos W', 'uu', '22122019092647scaled_1571830495599.jpg', 1, '2019-12-22 08:26:32', '2019-12-22 08:26:32', '2019-12-30 17:00:00'),
(35, 1, 6, 'INVORD122220193614', 'Polo', 'vvvv', '22122019101101scaled_1574263645132.jpg', 1, '2019-11-21 17:00:00', '2019-11-21 17:00:00', '2019-12-30 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `prod_id` int(10) UNSIGNED NOT NULL,
  `item_price` int(10) UNSIGNED NOT NULL,
  `item_quantity` int(10) UNSIGNED NOT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `item_note` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_details`
--

INSERT INTO `order_details` (`id`, `order_id`, `prod_id`, `item_price`, `item_quantity`, `item_total`, `item_note`, `created_at`, `updated_at`) VALUES
(108, 25, 1, 48000, 24, 1152000, 'M', '2019-12-22 04:19:00', '2019-12-22 04:19:00'),
(109, 25, 1, 48000, 10, 480000, 'panjang m', '2019-12-22 04:19:24', '2019-12-22 04:19:24'),
(133, 32, 2, 85000, 55, 4675000, 'g', '2019-12-22 07:55:39', '2019-12-22 07:55:39'),
(134, 32, 4, 50000, 24, 1200000, 't', '2019-12-22 07:55:47', '2019-12-22 07:55:47'),
(135, 33, 2, 85000, 25, 2125000, 'gg', '2019-12-22 08:03:00', '2019-12-22 08:03:00'),
(136, 34, 3, 120000, 28, 3360000, 'L', '2019-12-22 08:26:55', '2019-12-22 08:26:55'),
(137, 34, 3, 120000, 10, 1200000, 'M', '2019-12-22 08:27:02', '2019-12-22 08:27:02'),
(138, 35, 4, 50000, 10, 500000, 'b', '2019-12-22 09:11:13', '2019-12-22 09:11:13'),
(139, 35, 4, 50000, 12, 600000, 'l', '2019-12-22 09:11:21', '2019-12-22 09:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `pay_total` int(10) UNSIGNED NOT NULL,
  `pay_discount` int(10) UNSIGNED NOT NULL,
  `payment` int(10) UNSIGNED NOT NULL,
  `pay_kurang` int(10) UNSIGNED NOT NULL,
  `repayment` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `lunas` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `order_id`, `pay_total`, `pay_discount`, `payment`, `pay_kurang`, `repayment`, `lunas`, `created_at`, `updated_at`) VALUES
(11, 25, 1632000, 0, 1000000, 0, 632000, 1, '2019-12-22 04:19:41', '2019-12-22 08:27:39'),
(17, 32, 5875000, 0, 3000000, 2875000, 0, 0, '2019-12-22 07:56:01', '0000-00-00 00:00:00'),
(18, 33, 2125000, 0, 1100000, 0, 1025000, 1, '2019-12-22 08:03:19', '2019-12-22 08:50:15'),
(19, 34, 4540000, 20000, 2400000, 2140000, 0, 0, '2019-12-22 08:27:18', '0000-00-00 00:00:00'),
(20, 35, 1100000, 0, 600000, 0, 500000, 1, '2019-12-22 09:11:44', '2019-12-22 09:12:29');

-- --------------------------------------------------------

--
-- Table structure for table `productions`
--

CREATE TABLE `productions` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `suplier_id` int(10) UNSIGNED NOT NULL,
  `shop_note` text COLLATE utf8mb4_unicode_ci,
  `shop_total` int(10) UNSIGNED NOT NULL,
  `shop_discount` int(10) UNSIGNED NOT NULL,
  `profit` int(10) UNSIGNED NOT NULL,
  `shop_picture` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `productions`
--

INSERT INTO `productions` (`id`, `order_id`, `suplier_id`, `shop_note`, `shop_total`, `shop_discount`, `profit`, `shop_picture`, `created_at`, `updated_at`) VALUES
(1, 25, 3, 'murah', 840000, 50000, 792000, '22122019084841scaled_1576690220644.jpg', '2019-12-22 07:48:41', '2019-12-22 07:48:41'),
(2, 32, 1, 'eree', 3000000, 100000, 2875000, '22122019085708scaled_1575822118751.jpg', '2019-12-22 07:57:08', '2019-12-22 07:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `production_details`
--

CREATE TABLE `production_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `production_id` int(10) UNSIGNED NOT NULL,
  `item_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `item_quantity` int(10) UNSIGNED NOT NULL,
  `item_total` int(10) UNSIGNED NOT NULL,
  `item_note` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `prod_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prod_picture` text COLLATE utf8mb4_unicode_ci,
  `prod_price` int(10) UNSIGNED NOT NULL,
  `prod_counter` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `prod_visible` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `prod_name`, `prod_picture`, `prod_price`, `prod_counter`, `prod_visible`, `created_at`, `updated_at`) VALUES
(1, 17, 'Kaos Oblong 30s', '22122019001923', 48000, 300, 1, '2019-12-18 05:48:02', '2019-12-22 07:02:19'),
(2, 19, 'Sweeter', '18122019064927scaled_Screenshot_20190710-052440_1.jpg', 85000, 223, 1, '2019-12-18 05:49:27', '2019-12-22 08:03:00'),
(3, 18, 'Kemeja Japan', '18122019065039scaled_Screenshot_20190710-052312_1.jpg', 120000, 241, 1, '2019-12-18 05:50:39', '2019-12-22 08:27:02'),
(4, 24, 'Raglan Pendek', '20122019110606scaled_Screenshot_2019_09_25_12_44_49.jpg', 50000, 168, 1, '2019-12-20 10:06:06', '2019-12-22 09:11:21');

-- --------------------------------------------------------

--
-- Table structure for table `supliers`
--

CREATE TABLE `supliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `sup_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sup_counter` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `supliers`
--

INSERT INTO `supliers` (`id`, `sup_name`, `sup_address`, `sup_phone`, `sup_counter`, `created_at`, `updated_at`) VALUES
(1, 'Toko Busana Makmur', 'Sorogenen', '0274568956', 0, '2019-12-18 05:52:46', '2019-12-18 05:53:53'),
(2, 'Toko Sandang Sajahtera', 'Godean', '08963258411', 0, '2019-12-18 05:53:21', '2019-12-18 05:54:25'),
(3, 'Toko Kain Aris', 'Klitren', '027455588', 0, '2019-12-20 10:07:27', '2019-12-20 10:07:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` text COLLATE utf8mb4_unicode_ci,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `picture`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'asdjsanmdo39eu3hri3nr32n43ne', 'admin@email.com', '21232f297a57a5a743894a0e4a801fc3', '2019-12-03 14:01:56', '2019-12-03 14:01:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_id_foreign` (`user_id`),
  ADD KEY `orders_client_id_foreign` (`client_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_details_order_id_foreign` (`order_id`),
  ADD KEY `order_details_prod_id_foreign` (`prod_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payments_order_id_foreign` (`order_id`);

--
-- Indexes for table `productions`
--
ALTER TABLE `productions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productions_suplier_id_foreign` (`suplier_id`),
  ADD KEY `productions_order_id_foreign` (`order_id`);

--
-- Indexes for table `production_details`
--
ALTER TABLE `production_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_production_id` (`production_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_cat_id_foreign` (`cat_id`);

--
-- Indexes for table `supliers`
--
ALTER TABLE `supliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `productions`
--
ALTER TABLE `productions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `production_details`
--
ALTER TABLE `production_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `supliers`
--
ALTER TABLE `supliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_client_id_foreign` FOREIGN KEY (`client_id`) REFERENCES `clients` (`id`),
  ADD CONSTRAINT `orders_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_details_prod_id_foreign` FOREIGN KEY (`prod_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `payments`
--
ALTER TABLE `payments`
  ADD CONSTRAINT `payments_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `productions`
--
ALTER TABLE `productions`
  ADD CONSTRAINT `productions_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `productions_suplier_id_foreign` FOREIGN KEY (`suplier_id`) REFERENCES `supliers` (`id`);

--
-- Constraints for table `production_details`
--
ALTER TABLE `production_details`
  ADD CONSTRAINT `fk_production_id` FOREIGN KEY (`production_id`) REFERENCES `productions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_cat_id_foreign` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
